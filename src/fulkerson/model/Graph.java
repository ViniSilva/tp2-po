package fulkerson.model;

import java.util.Stack;

public class Graph {

	private float[][] edges;
	private int[][] incidencyMatrix;
	private Float[] originalCostVector;

	//the number of vertices
	private int size;
	private int numEdges;

	//-----------------------------------------------------------------------------------
	public Graph(int size){
		this.size=size;
		this.edges = new float[size][size];
	}

	//-----------------------------------------------------------------------------------
	public Graph(float[][] adjMatrix){

		//Como a matriz e quadrada, o o numero de vertices no grafo e igual a qualquer uma das dimensoes da matriz.
		this.size = adjMatrix[0].length;
		this.edges = adjMatrix;
	}

	//-----------------------------------------------------------------------------------
	public float[][] getAdjMatrix(){
		return edges;
	}

	//-----------------------------------------------------------------------------------
	public int getSize(){
		return this.size;
	}

	//-----------------------------------------------------------------------------------
	public void setIncidencyMatrix(int [][] incMatrix){
		this.incidencyMatrix = incMatrix;
	}

	//-----------------------------------------------------------------------------------
	public int[][] getIncidencyMatrix(){
		return this.incidencyMatrix;
	}

	public void setNumEdges(int numEdges){
		this.numEdges = numEdges;
	}

	//-----------------------------------------------------------------------------------
	public int getNumEdges(){
		return this.numEdges;
	}
	
	//-----------------------------------------------------------------------------------
	public void setCostVector(Float[] costVector){
		this.originalCostVector = costVector;
	}
	
	//-----------------------------------------------------------------------------------
	public Float[] getCostVector(){
		return this.originalCostVector;
	}


	//-----------------------------------------------------------------------------------

	//Funcao usada como teste para referencia
//	public boolean breadthFirstSearch(int source, int destination, int path[]){
//
//		boolean visited[] = new boolean[this.size];
//
//		//Inicializando o vetor de visitados como zero.
//		for(int i=0; i<this.size; i++){
//			visited[i] = false;
//		}
//
//		//Fila para busca em largura.
//		LinkedList<Integer> queue = new LinkedList<Integer>();
//
//		queue.add(source);
//
//
//		visited[source] = true;
//
//		path[source] = -1;
//
////		//print matriz de Adj de residual:
////		System.out.println("\nMatriz de adj da residual: ");
////		for(int i=0; i<this.size; i++){
////			for(int j=0; j<this.size; j++){
////				System.out.print(this.getAdjMatrix()[i][j] + " ");
////			}
////			System.out.println();
////		}
////		System.out.println("\n");
//
//		//Busca em largura propriamente dita
//		while(queue.size()!=0){
//
//			//busca um vertice da fila
//			int currentVertex = queue.poll();
//
//			//percorre a lista de vertices procurando aqueles que ainda n�o foram visitados e estao conectados com currentVertex
//			for(int i=0; i<this.size; i++){
//
//
//				if(visited[i]==false && this.getAdjMatrix()[currentVertex][i] > 0){
//					queue.add(i);
//					path[i] = currentVertex;
//					visited[i] = true;
//				}
//			}
//		}
//
//		System.out.println("Path encontrado pela bfs ");
//		for(int i=0; i<path.length; i++){
//			System.out.print(path[i] + " ");
//		}
//
//		//se chegamos ao vertice destino, retornamos true. Caso contratrio, retornamos falso.
//		if(visited[destination]){
//			return true;
//		}else{
//			return false;
//		}
//	}

	


	//-----------------------------------------------------------------------------------
	public boolean iteractiveDfs(int source, int destination, int[] path, boolean[] visited){

		if(visited==null){
			visited = new boolean[this.size];
		}
		

		//Inicializando o vetor de visitados como zero.
		for(int i=0; i<this.size; i++){
			visited[i] = false;
		}

		Stack<Integer> stack = new Stack<>();
		stack.push(source);

		if(path!=null){
			path[source]=-1;
		}
		

//		//print matriz de Adj de residual:
//		System.out.println("\nMatriz de adj da residual: ");
//		for(int i=0; i<this.size; i++){
//			for(int j=0; j<this.size; j++){
//				System.out.print(this.getAdjMatrix()[i][j] + " ");
//			}
//			System.out.println();
//		}
//		System.out.println("\n");


		while(!stack.isEmpty()){
			source = stack.pop();

			if(!visited[source]){
				visited[source]=true;

				for(int i=0; i<this.size; i++){
					if(visited[i]==false && this.getAdjMatrix()[source][i] > 0){
						stack.push(i);
						if(path!=null){
							path[i] = source;
						}
						
					}
				}
			}
		}

//		if(path!=null){
//			System.out.println("Path encontrado pela dfsIterativa: ");
//			
//			for(int i=0; i<path.length; i++){
//				System.out.print(path[i] + " ");
//			}
//		}
		

		return visited[destination];
	}

	//-----------------------------------------------------------------------------------
	//funcao usada somente no grafo residual.
	public EdgeList getMinCut(int s, Graph originalGraph){
		
		EdgeList stCut = new EdgeList();
		boolean visited[] = new boolean[this.size];
		/*passamos source, 
		 *um numero que nao faz diferenca, pois nao estamos interessados se e possivel chegar ao destino 
		 * null, pois nao estamos interessados no caminho que ele vai alterar
		 * visited pois estamos interessados nos vertices que serao visitados
		 */
		this.iteractiveDfs(s, 0, null, visited);
		
		for(int i=0; i<this.size; i++){
			for(int j=0; j<this.size; j++){
				if(visited[i] && !visited[j] && originalGraph.getAdjMatrix()[i][j] > 0){
					stCut.addEdge(i, j);
				}
			}
		}

		return stCut;
	}


}
