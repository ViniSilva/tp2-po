package fulkerson.model;

import java.util.ArrayList;
import java.util.List;

public class EdgeList {

	private List<PathEdge> list;

	// -----------------------------------------------------------------------------------
	public EdgeList() {
		this.list = new ArrayList<PathEdge>();
	}

	// -----------------------------------------------------------------------------------
	public void addEdge(int emmiter, int receiver) {

		PathEdge newEdge = new PathEdge(emmiter, receiver);
		this.list.add(newEdge);
	}

	// -----------------------------------------------------------------------------------
	public void clearList() {
		this.list.clear();
	}

	// -----------------------------------------------------------------------------------
	public boolean isEdgeUsedInAPath(int edge, Graph graph) {

		for (PathEdge pathEdge : this.list) {

			if (graph.getIncidencyMatrix()[pathEdge.getSender()][edge] < 0
					&& graph.getIncidencyMatrix()[pathEdge.getReceiver()][edge] > 0) {
				return true;
			}
		}

		return false;
	}

	// -----------------------------------------------------------------------------------
	public PathEdge getEdgeByIndex(int edge, Graph graph) {

		for (PathEdge pathEdge : this.list) {

			if (graph.getIncidencyMatrix()[pathEdge.getSender()][edge] < 0
					&& graph.getIncidencyMatrix()[pathEdge.getReceiver()][edge] > 0) {
				return pathEdge;
			}
		}

		return null;
	}

	// -----------------------------------------------------------------------------------
	public boolean containsEdge(int sender, int receiver) {

		for (PathEdge pathEdge : this.list) {
			if (pathEdge.getSender() == sender && pathEdge.getReceiver() == receiver) {
				return true;
			}
		}
		return false;
	}
	
	// -----------------------------------------------------------------------------------
	public List<PathEdge> getRawList(){
		
		return this.list;
	}

}
