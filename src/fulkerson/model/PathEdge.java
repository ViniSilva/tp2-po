package fulkerson.model;

public class PathEdge {
	private int sender;
	private int receiver;
	private float flow;
	
	public PathEdge(int sender, int receiver){
		this.sender = sender;
		this.receiver = receiver;
		this.flow=0;
	}
	
	public int getSender(){
		return this.sender;
	}
	
	public int getReceiver(){
		return this.receiver;
	}
	
	public void updateFlow(float increment){
		this.flow += increment;
	}
	
	public float getFlow(){
		return this.flow;
	}
}
