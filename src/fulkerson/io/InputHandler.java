package fulkerson.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

import fulkerson.model.Graph;

public class InputHandler {
	
	private Scanner scanner;
	
	//-------------------------------------------------------------------------------------------------------------------
	public InputHandler(String fileName){
		
		File file = new File(fileName);
		try {
			this.scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// -------------------------------------------------------------------------------------------------------------------
	public Graph readInput(){
		
		int nRows=0;
		int nCols=0;
		
		
		try {
			
			
			//Numero de pontos
			nRows=Integer.parseInt(this.scanner.nextLine());
			
			//Numero de subconjuntos
			nCols=Integer.parseInt(this.scanner.nextLine());
			
			Float[] edgeWeights = new Float[nCols];
			
			String edgeWeightsStr = this.scanner.nextLine();
			
			edgeWeights = Arrays.stream(edgeWeightsStr.split(" ")).map((strWeight)-> Float.parseFloat(strWeight)).toArray(Float[]::new);
			
			
			//ler a matriz de incidencia para a mem�ria
			int[][] incidencyMatrix=new int[nRows][nCols];
			
			for(int i=0; i<nRows; i++){
				for(int j=0; j<nCols; j++){
					incidencyMatrix[i][j] = this.scanner.nextInt();
					
				}
			}
			
			
			//matriz de adjacencia -> inicialmente vazia
			float[][] adjMatrix = new float[nRows][nRows];
			
			
			int outVertex=-1;
			int inVertex=-1;
			
			
			//ler as colunas da matriz de incidencia para montar a matriz de adjacencia
			int readValue=0;
			
			//i -> colunas
			for(int i=0; i<nCols; i++){
				
				//j -> linhas
				for(int j=0; j<nRows; j++){
					
					readValue=incidencyMatrix[j][i];
					
					//aresta sai do vertice j i
					if(readValue<0){
						outVertex=j;
						
					}
					
					if(readValue>0){
						inVertex=j;
					}
				}
				
				//Se aresta for repetida, somar as capacidades
				adjMatrix[outVertex][inVertex] += edgeWeights[i];
				
			}
			
			//printar matriz de adj.
//			System.out.println("Matriz de adjacencia do grafo original: ");
//			for(int i=0; i<nRows; i++){
//				for(int j=0; j<nRows; j++){
//					System.out.print(adjMatrix[i][j] + " ");
//				}
//				System.out.print("\n");
//			}
//			System.out.println("\n\n");
			
			
			Graph readGraph=new Graph(adjMatrix);
			readGraph.setIncidencyMatrix(incidencyMatrix);
			readGraph.setNumEdges(nCols);
			readGraph.setCostVector(edgeWeights);
			
			return readGraph;
			
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return null;
	}

}
