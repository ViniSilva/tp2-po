package fulkerson.main;

import fulkerson.controller.FordFulkerson;
import fulkerson.io.InputHandler;
import fulkerson.model.Graph;

public class FulkersonMain {
	
	
	
	public static void main(String[] args){
		InputHandler input = new InputHandler("ford.txt");
		
		Graph readGraph = input.readInput();
		
		FordFulkerson ff = new FordFulkerson(readGraph, 0, readGraph.getSize()-1);
		
		ff.solveMaxFlow();
		
	}
	

}
