package fulkerson.controller;

import fulkerson.model.EdgeList;
import fulkerson.model.Graph;
import fulkerson.model.PathEdge;;

public class FordFulkerson {
	private Graph graph;
	private int source;
	private int destination;
	
	//-----------------------------------------------------------------------------------
	public FordFulkerson(Graph graph, int source, int destination){
		this.graph = graph;
		this.source = source;
		this.destination = destination;
		
	}
	
	//-----------------------------------------------------------------------------------
	public void solveMaxFlow(){
		
		EdgeList actualPath = new EdgeList();
		
		EdgeList edgesWithFlow = new EdgeList();
		
		//apenas indices
		int u=0;
		int v=0;
		
		Graph residualGraph = new Graph(this.graph.getSize());
		
		//Copiar o grafo original no grafo residual
		for(u=0; u<this.graph.getSize(); u++){
			for(v=0; v<this.graph.getSize(); v++){
				residualGraph.getAdjMatrix()[u][v] = this.graph.getAdjMatrix()[u][v];
			}
		}
		
		//caminho entre s e t no grafo residual. Inicialmente vazio.
		int[] residualPath = new int[this.graph.getSize()];
		
		for(int i=0; i<residualPath.length; i++){
			residualPath[i]=0;
		}
		
		
		//inicialmente o fluxo m�ximo � zero
		float max_flow=0;
		
		//O fluxo qque passa pelo caminho corrente.
		float path_flow=0;
		
		//Iterar ate que nao seja mais possivel encontrar caminhos no grafo residual
		while(residualGraph.iteractiveDfs(this.source, this.destination, residualPath, null)){
			
			//zera o caminho a cada iteracao, pois ele sera diferente
			actualPath.clearList();
		
			path_flow=Float.MAX_VALUE;
			
			//Encontra a menor capacidade no caminho atual.
			
			for(v=this.destination; v!=this.source; v=residualPath[v]){
				
				u=residualPath[v];
				
				path_flow = Math.min(path_flow, residualGraph.getAdjMatrix()[u][v]);
				
				actualPath.addEdge(u,v);
				
				if(!edgesWithFlow.containsEdge(u, v)){
					edgesWithFlow.addEdge(u, v);
				}
				
			}
			
			
			//atualiza as capacidades residuais das arestas e inverte o sentido das arestas ao longo do caminho.
			for(v=this.destination; v!=this.source; v=residualPath[v]){
				
				u=residualPath[v];
				residualGraph.getAdjMatrix()[u][v] -= path_flow;
				
				residualGraph.getAdjMatrix()[v][u] += path_flow;
				
			}
			
			//atualiza o fluxo para as arestas com fluxo:
			for(PathEdge pEdge : edgesWithFlow.getRawList()){
				
				if(actualPath.containsEdge(pEdge.getSender(), pEdge.getReceiver())){
					pEdge.updateFlow(path_flow);
				}
			}
			
			max_flow+=path_flow;
			
			
//			System.out.println("Imprimindo lista de arestas usadas");
//			for(PathEdge pathEdge : actualPath){
//				System.out.println("Sender: " + pathEdge.getSender() + " Receiver: " + pathEdge.getReceiver());
//			}
//			System.out.println("\n\n\n");
			
			//print caminho sendo considerado
			for(int i=0; i<this.graph.getNumEdges(); i++){
				
				if(actualPath.isEdgeUsedInAPath(i, this.graph)){
					System.out.print("1 ");
				}else{
					System.out.print("0 ");
				}
			}
			
			System.out.print("\n");
			
			//print fluxo em cada aresta
			PathEdge aux;
			for(int i=0; i<this.graph.getNumEdges(); i++){
				aux = edgesWithFlow.getEdgeByIndex(i, this.graph);
				if(aux!=null){
					System.out.print((int)aux.getFlow() + " ");
				}else{
					System.out.print("0 ");
				}
			}
			
			System.out.print("\n");
			
			//print vetor C original:
			for(int i=0; i<this.graph.getCostVector().length; i++){
				System.out.printf("%.3f", this.graph.getCostVector()[i]);
				System.out.print(" ");
			}
			System.out.println("\n");
			
		}
	
		System.out.println("Fluxo Maximo: " + (int)max_flow);
	
		
		
		EdgeList stCut = residualGraph.getMinCut(this.source, graph);

		
		System.out.print("Corte Minimo: ");
		for(int i=0; i<this.graph.getNumEdges(); i++){
			if(stCut.isEdgeUsedInAPath(i, this.graph)){
				System.out.print("1 ");
			}else{
				System.out.print("0 ");
			}
		}
		
		
	}
	
	

}
