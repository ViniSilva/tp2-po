package setcover.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import setcover.controller.PrimalDual;
import setcover.model.Point;
import setcover.model.Subset;


public class InputHandler {
	
	private BufferedReader input;

	private String fileName;

	// -------------------------------------------------------------------------------------------------------------------
	public InputHandler(String filename) {
		this.fileName = filename;
	}
	
	// -------------------------------------------------------------------------------------------------------------------
	public PrimalDual readInput(){
		
		List<Point> points = new ArrayList<Point>();
		List<Subset> subsets = new ArrayList<Subset>();
		
		int nRows=0;
		int nCols=0;
		
		try {
			FileReader fr = new FileReader(this.fileName);
			this.input = new BufferedReader(fr);
			
			//Numero de pontos
			nRows=Integer.parseInt(this.input.readLine());
			
			//Numero de subconjuntos
			nCols=Integer.parseInt(this.input.readLine());
			
			Float[] setWeights = new Float[nCols];
			
			char readChar=' ';
			
			
			//lendo o vetor de pesos dos conjuntos
//			for(int i=0; i<nCols;){
//				
//				readChar = (char) input.read();
//				
//				if(readChar!=' ' && readChar!='\r' && readChar!='\n'){
//					setWeights[i] = Integer.parseInt(String.valueOf(readChar));
//					subsets.add(new Subset(setWeights[i]));
//					i++;
//				}
//			}
			
			
			String setWeightsStr = input.readLine();
			
			setWeights = Arrays.stream(setWeightsStr.split(" ")).map((strWeight)-> Float.parseFloat(strWeight)).toArray(Float[]::new);
			
			Arrays.stream(setWeights).forEachOrdered((weight) -> subsets.add(new Subset(weight)));
			
			
			
			//cria a lista de pontos disponiveis
			for(int i=0; i<nRows; i++){
				points.add(new Point());
			}
			
			Point.resetNextIDs();
			
			Subset auxSubset;
			Point auxPoint;
			
			for(int i=0; i<nRows; i++){
				
				//Um ponto por linha
				auxPoint = new Point();
				
				for(int j=0; j<nCols;){
					
					readChar = (char)input.read();
					
					if(readChar=='1' || readChar=='0'){
						if(readChar=='1'){
							auxSubset = subsets.get(j);
							
							auxSubset.addPoint(auxPoint);
						}
						j++;
					}
				}
			}
			
			PrimalDual pd = new PrimalDual(points, subsets);
			
			return pd;
					
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}

}
