package setcover.main;

import setcover.controller.PrimalDual;
import setcover.io.InputHandler;
import setcover.model.SubsetList;

public class SetCoverMain {
	
	public static void main(String[] args){
		InputHandler inputHandler = new InputHandler("setcover.txt");
		PrimalDual targetProblem = inputHandler.readInput();
		
		SubsetList cover = targetProblem.solveSetCover();
		
		System.out.println(cover.getSumOfWeights());
	}

}
