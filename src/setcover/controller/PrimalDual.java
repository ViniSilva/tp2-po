package setcover.controller;

import java.util.List;

import setcover.model.Point;
import setcover.model.Subset;
import setcover.model.SubsetList;

public class PrimalDual {
	
	private List<Point> points;
	private SubsetList subsets;
	private SubsetList cover;
	
	//-----------------------------------------------------------------------------------
	public PrimalDual(List<Point> points, List<Subset> subsets){
		this.points = points;
		this.subsets = new SubsetList(subsets);
		this.cover = new SubsetList();
		
	}
	
	//-----------------------------------------------------------------------------------
	private void printCurrentCover(){
		
		
		Subset aux;
		for(int i=0; i<this.subsets.getLength(); i++){
			
			aux = this.subsets.getSubset(i);
			if(aux!=null){
				
				if(this.cover.containsSetOfID(aux.getID())){
					System.out.print("1 ");
				}else{
					System.out.print("0 ");
				}
			}
		}
	}
	
	//-----------------------------------------------------------------------------------
	private void printCurrentVerticesWeights(){
		
		for(Point p : this.points){
			System.out.printf("%.3f", p.getWeight());
			System.out.print(" ");
		}
		
	}
	
	//-----------------------------------------------------------------------------------
	public SubsetList solveSetCover(){
		
		List<Subset> auxSetList;
		Subset chosen;
		
		for(Point p : points){
			
			
			
			
			//este ponto pertence a algum conjunto da cobertura?
			if(!this.cover.getSubsetsContainingPoint(p.getID()).isEmpty()){
				continue;
			}else{
				
				auxSetList = this.subsets.getSubsetsContainingPoint(p.getID());
				
				chosen = auxSetList.get(0);
				
				for(Subset auxSubset : auxSetList){
					if(auxSubset.getTotalWeight() <= chosen.getTotalWeight()){
						if(auxSubset.getElementsWeight() < auxSubset.getTotalWeight()){
							chosen = auxSubset;
						}else{
							continue;
						}
					}else{
						continue;
					}
				}
				
				p.setWeight(chosen.getTotalWeight() - chosen.getElementsWeight());
				
				//update the weights of the points in each subset:
				for(Subset subset: subsets.getSubsets()){
					subset.updateWeights(this.points);
				}
				
				cover.addSubset(chosen);
			}
			
			//print vetor y -> Cobertura selecionada ate o momento
			this.printCurrentCover();
			
			System.out.print("\n");
			//print vetor x -> pesos
			this.printCurrentVerticesWeights();
			System.out.print("\n\n\n");
			
			
		}
		
		return cover;
	}
}
