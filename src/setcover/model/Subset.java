package setcover.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Subset {
	
	private int ID;
	private List<Point> points;
	private float totalWeight;
	
	private static int instances=0;
	
	//-----------------------------------------------------------------------------------
	public Subset(float pesoTotal){
		this.ID = instances;
		instances++;
		this.points = new ArrayList<Point>();
		this.totalWeight=pesoTotal;
	}
	
	//-----------------------------------------------------------------------------------
	public Point getPointByID(int pointID){
		
		for(Point point : points){
			if(point.getID() == pointID){
				return point;
			}
		}
		
		return null;
	}
	
	//-----------------------------------------------------------------------------------
	public void updateWeights(List<Point> newPoints){
		
		for(Point p : newPoints){
			
			Point localPoint = this.getPointByID(p.getID());
			if(localPoint!=null){
				localPoint.setWeight(p.getWeight());
			}
		}
	}
	
	//-----------------------------------------------------------------------------------
	public Point getPointByIndex(int index){
		
		return this.points.get(index);
		
	}
	
	//-----------------------------------------------------------------------------------
	public int getSetSize(){
		return this.points.size();
	}
	
	//-----------------------------------------------------------------------------------
	public void addPoint(Point p){
		this.points.add(p);
	}
	
	//-----------------------------------------------------------------------------------
	public int getID(){
		return this.ID;
	}

	//-----------------------------------------------------------------------------------
	public void setTotalWeight(int totalWeight){
		this.totalWeight=totalWeight;
	}
	
	//-----------------------------------------------------------------------------------
	public float getTotalWeight(){
		return totalWeight;
	}
	
	//-----------------------------------------------------------------------------------
	public float getElementsWeight(){
		
		List<Float> elementsWeight = this.points.stream().map((element) -> {
			return element.getWeight();
		}).collect(Collectors.toList());
		
		float weightSum = elementsWeight.stream().reduce(0f, (peso1, peso2) -> peso1 + peso2); 
		
		return weightSum;
	}
}
