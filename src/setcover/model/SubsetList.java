package setcover.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SubsetList {
	
	private List<Subset> subsets;
	
	//-----------------------------------------------------------------------------------
	public SubsetList(){
		this.subsets = new ArrayList<Subset>();
	}
	
	//-----------------------------------------------------------------------------------
	public SubsetList(List<Subset> subsets){
		this.subsets = subsets;
	}
	
	//-----------------------------------------------------------------------------------
	public void addSubset(Subset newSubset){
		this.subsets.add(newSubset);
	}
	
	//-----------------------------------------------------------------------------------
	public Subset getSubset(int index){
		
		try{
			return this.subsets.get(index);
		}catch(IndexOutOfBoundsException exception){
			return null;
		}
		
	}
	
	//-----------------------------------------------------------------------------------
	public int getLength(){
		return this.subsets.size();
	}
	
	//-----------------------------------------------------------------------------------
	public float getSumOfWeights(){
		
		List<Float> weights = subsets.stream().map((subset) -> 
			subset.getTotalWeight()
		).collect(Collectors.toList());
		
		int sumOfWeights = weights.stream().reduce(0f, (accumulator, currentWeight) -> accumulator + currentWeight).intValue();
		
		
		return sumOfWeights;
	}
	
	//-----------------------------------------------------------------------------------
	public List<Subset> getSubsetsContainingPoint(int pointID){
		
		List<Subset> setsContainingPoint = new ArrayList<Subset>();
		
		for(Subset subset : subsets){
			if(subset.getPointByID(pointID)!=null){
				setsContainingPoint.add(subset);
			}
		}
		return setsContainingPoint;
	}
	
	//-----------------------------------------------------------------------------------
	public boolean containsSetOfID(int id){
		
		for(Subset subset:this.subsets){
			if(subset.getID() == id){
				return true;
			}
		}
		return false;
	}
	
	public List<Subset> getSubsets(){
		return this.subsets;
	}
	
	

}
