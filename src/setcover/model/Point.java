package setcover.model;

public class Point {
	
	private int ID;
	private float weight;
	private static int nextID=0;
	
	//-----------------------------------------------------------------------------------
	public Point(int peso){
		this.ID = nextID;
		nextID++;
		this.weight=peso;
	}
	
	//-----------------------------------------------------------------------------------
	public Point(){
		this.ID = nextID;
		nextID++;
		this.weight=0;
	}
	
	//-----------------------------------------------------------------------------------
	public float getWeight(){
		return this.weight;
	}
	
	//-----------------------------------------------------------------------------------
	public void setWeight(float peso){
		this.weight = peso;
	}
	
	//-----------------------------------------------------------------------------------
	public int getID(){
		return this.ID;
	}
	
	//-----------------------------------------------------------------------------------
	public static void resetNextIDs(){
		nextID=0;
	}
	
	//-----------------------------------------------------------------------------------
	@Override
	public boolean equals(Object obj){
		
		if(obj instanceof Point){
			Point castObj = (Point)obj;
			
			if(castObj.ID == this.ID){
				return true;
			}
		}
		return false;
	}

}
